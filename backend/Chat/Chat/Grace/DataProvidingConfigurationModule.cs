﻿using Grace.DependencyInjection;
using Serilog;

namespace Chat.Grace
{
    public class DataProvidingConfigurationModule : IConfigurationModule
    {
        public void Configure(IExportRegistrationBlock r)
        {
            r.ExportFactory<StaticInjectionContext, ILogger>(LoggerFactory);
        }
        
        private ILogger LoggerFactory(StaticInjectionContext context)
        {
            return Log.Logger.ForContext("Context", context.TargetInfo?.InjectionType?.Name != null 
                ? $" [{context.TargetInfo.InjectionType.Name}]" 
                : " [Middleware]");
        }
    }
}