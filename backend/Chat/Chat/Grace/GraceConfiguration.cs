﻿using Grace.DependencyInjection;

namespace Chat.Grace
{
    public class GraceConfiguration : IConfigurationModule
    {
        public void Configure(IExportRegistrationBlock r)
        {
            r.AddModule(new DataProvidingConfigurationModule());
        }
    }
}