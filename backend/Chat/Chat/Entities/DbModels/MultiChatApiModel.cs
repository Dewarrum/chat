﻿using System;

namespace Chat.Entities.DbModels
{
    public class MultiChatApiModel
    {
        public string DisplayName { get; set; }
        public Guid ChatId { get; set; }
        public DateTime LastMessageAt { get; set; }
        public string AvatarUrl { get; set; }
        public string ChatHistoryPath { get; set; }
    }
}