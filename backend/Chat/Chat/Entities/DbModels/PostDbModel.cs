﻿using System;

namespace Chat.Entities.DbModels
{
    public class PostDbModel
    {
        public Guid AuthorId { get; set; }
        public string Text { get; set; }
        public int Likes { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}