﻿using System;
using System.Collections.Generic;

namespace Chat.Entities.DbModels
{
    public class UserDbModel
    {
        public string Login { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public Guid UserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastActive { get; set; }
        public string Status { get; set; }
        
        public string AvatarUrl { get; set; }
        public string Url { get; set; }
        public string HtmlUrl { get; set; }

        public byte[] Password { get; set; }
        public byte[] Salt { get; set; }
        // [refresh token] = expiration date
        public Dictionary<string, DateTime> RefreshTokenMap { get; set; }
    }
}