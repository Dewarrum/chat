﻿using System;

namespace Chat.Entities.DbModels
{
    public class SingleChatDbModel
    {
        public Guid ChatId { get; set; }
        public DateTime LastMessageAt { get; set; }
        public string ChatHistoryPath { get; set; }
    }
}