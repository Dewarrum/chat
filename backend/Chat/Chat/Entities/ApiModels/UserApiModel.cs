﻿using System;

namespace Chat.Entities.ApiModels
{
    public class UserApiModel
    {
        public string Login { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public Guid UserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastActive { get; set; }
        public string Status { get; set; }
        
        public string AvatarUrl { get; set; }
        public string Url { get; set; }
        public string HtmlUrl { get; set; }
        public string FriendsUrl { get; set; }
        public string FollowersUrl { get; set; }
        public string PostsUrl { get; set; }
        public string DialogsUrl { get; set; }
    }
}