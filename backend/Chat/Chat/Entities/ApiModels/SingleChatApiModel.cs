﻿using System;

namespace Chat.Entities.ApiModels
{
    public class SingleChatApiModel : ChatApiModel
    {
        public string ConversationalistUrl { get; set; }
    }
}