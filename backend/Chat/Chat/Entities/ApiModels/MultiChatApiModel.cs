﻿using System;

namespace Chat.Entities.ApiModels
{
    public class MultiChatApiModel : ChatApiModel
    {
        public string ConversationalistsUrl { get; set; }
        public string DisplayName { get; set; }
        public string AvatarUrl { get; set; }
    }
}