﻿using System;

namespace Chat.Entities.ApiModels
{
    public class PostApiModel
    {
        public string AuthorUrl { get; set; }
        public string Text { get; set; }
        public int Likes { get; set; }
        public string LikedUsersUrl { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}