﻿using System;

namespace Chat.Entities.ApiModels
{
    public class ChatApiModel
    {
        public Guid ChatId { get; set; }
        public DateTime LastMessageAt { get; set; }
    }
}