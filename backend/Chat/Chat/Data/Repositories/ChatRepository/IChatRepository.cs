﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Chat.Entities.ApiModels;

namespace Chat.Data.Repositories.ChatRepository
{
    public interface IChatRepository
    {
        Task<IEnumerable<ChatApiModel>> GetWithParameters(int skip = 0, int take = 10);
        Task<ChatApiModel> Get(Guid chatId);
        Task<ChatApiModel> Create(Guid chatId);
        Task<ChatApiModel> Delete(Guid chatId);
    }
}