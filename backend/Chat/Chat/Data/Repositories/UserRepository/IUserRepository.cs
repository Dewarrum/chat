﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Chat.Entities.ApiModels;
using Chat.Entities.DbModels;

namespace Chat.Data.Repositories.UserRepository
{
    public interface IUserRepository
    {
        Task<IEnumerable<UserDbModel>> GetWithParameters(int skip = 0, int take = 10);
        
        Task<UserDbModel> Get(Guid userId);

        Task<UserDbModel> Create(string login, string password, string displayName,
            Dictionary<string, string> requisites);

        Task<UserDbModel> Delete(Guid userId);

        Task<UserDbModel> Update(Guid userId, Dictionary<string, string> requisites);

        Task<IEnumerable<UserDbModel>> GetFriends(Guid userId);

        Task<IEnumerable<UserDbModel>> GetFollowers(Guid userId);

        Task<IEnumerable<PostDbModel>> GetPosts(Guid userId);

        Task<IEnumerable<ChatApiModel>> GetChats(Guid userId);
    }
}